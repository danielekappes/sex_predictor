# Test - Data Scientist (profile statistician)
## Challenge


## 1. Create and activate virtual environment

Create virtual environment named `env-sex_predictor` (you may use other name)
```bash
python virtualenv env-sex_predictor
cd env-sex_predictor
cd Scripts
activate
cd ..
cd ..
```

## 2. Install python libraries

```bash
pip install -r requirements.txt
```

## 3. Execute python script to predict if a batch of patients are male or female (it will be written in file newsample_PREDICTIONS_DanielePereiraKappes.csv and printed in terminal)
```bash
python sex_predictor.py --input_file newsample.csv
```

## Observations
```bash
nemsample.csv file should be in same directory as sex_predictor.py script
It was used test_data_CANDIDATE.csv file without sex column as newsample.csv to test the sex_predictor.py script.
Python version used: 3.7.4
```