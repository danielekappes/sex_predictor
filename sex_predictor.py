# %%
import pandas as pd
import sys
import pickle
import numpy as np

# %%
filename = sys.argv[-1]
df_to_predict = pd.read_csv(filename)
features_logisticRegression = ['thal_2', 'chol', 'hc_1', 'hc_0', 'thal_3', 'age', 'trestbps', 'oldpeak', 'trf', 'thalach', 'exang', 'ca_0', 'sk_2', 'sk_1', 'ca_3', 'sk_3', 'ca_2', 'restecg_1', 'sk_0', 'restecg_0', 'thal_1', 'ca_1', 'restecg_2', 'ca_4']
# Criacao das variaveis dummy
# Essa logica e necessaria porque nao se pode garantir que o arquivo
# newsample.csv tera sempre todas as categorias de todas as variaveis categoricas
list_dummy = ['thal_2','hc_1','hc_0','thal_3','ca_0','sk_2','sk_1','ca_3','sk_3','ca_2','restecg_1','sk_0','restecg_0','thal_1','ca_1','restecg_2','ca_4']
for dummy in list_dummy:
    column = dummy.split('_')[0]
    value = int(dummy.split('_')[1])
    new_column_name = column+'_'+str(value)
    df_to_predict[new_column_name] = np.where(df_to_predict[column]== value, 1, 0)
    
df_dummies = df_to_predict[features_logisticRegression]
df_dummies.fillna(df_dummies.mean(), inplace=True)
loaded_model = pickle.load(open('logisticRegression_model.pkl', 'rb'))
predicted_values = loaded_model.predict(df_dummies)
pd.DataFrame(predicted_values, columns=['predicted_sex']).to_csv('newsample_PREDICTIONS_DanielePereiraKappes.csv')
print(predicted_values)
